package com.luigivampa92.yms.fintracker.routing.base

interface BindableRouting {
    fun bind()
    fun unbind()
}