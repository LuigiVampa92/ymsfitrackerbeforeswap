package com.luigivampa92.yms.fintracker.routing.base

object Screens {
    const val BALANCE = "BalanceFragment"
    const val SETTINGS = "SettingsFragment"
    const val INFO = "InfoFragment"

    const val EMAIL = "ExternalEmail"
    const val VK = "ExternalVk"
    const val TG= "ExternalTelegram"

    const val ACCOUNTS = "AccountFragment"
    const val ADD_RECORD = "AddRecordFragment"
}