package com.luigivampa92.yms.fintracker

object Constants {
    const val packageNameVk = "com.vkontakte.android"
    const val packageNameTg = "org.telegram.messenger"
    const val packageNameGmail = "com.google.android.gm"

    const val contactVk = "https://vk.com/id54957320"
    const val contactTg= "https://t.me/luigivampa92"
    const val contactEmail = "luigivampa92@gmail.com"

    const val exchangeRateBaseUrl = "http://data.fixer.io/api/"
    const val exchangeRateApiKey = "7134aefa2365c4706fee431ec3a2127e"

    val supportedCurrencies = listOf("USD", "EUR", "RUB")
}