package com.luigivampa92.yms.finops.model

enum class OperationType {
    IN,
    OUT
}