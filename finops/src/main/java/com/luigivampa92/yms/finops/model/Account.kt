package com.luigivampa92.yms.finops.model

data class Account (
        val name: String,
        val currency: Currency
)