package com.luigivampa92.yms.finops.model

data class Balance (
        val amount: Double,
        val currency: Currency
)